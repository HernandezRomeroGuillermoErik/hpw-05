function crear_input(tipo){
 var obj_temporal=document.createElement('input');
 obj_temporal.setAttribute("class","form-control");
 obj_temporal.type=tipo;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
 }
 
 function crear_input_e(tipo){
 var obj_temporal=document.createElement('input');
 obj_temporal.type=tipo;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
 }
 
function crear_input_password(tipo,placeholder){
 var obj_temporal=document.createElement('input');
 obj_temporal.setAttribute("class","form-control");
 obj_temporal.type=tipo;
 obj_temporal.placeholder=placeholder;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}

 function crear_radio(tipo){
 var obj_temporal=document.createElement('input');
 obj_temporal.setAttribute("class","form-control");
 obj_temporal.type=tipo;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}

function crear_sel(tipo,v){
 var obj_temporal=document.createElement('input');
 obj_temporal.setAttribute("class","form-control");
 obj_temporal.type=tipo;
 obj_temporal.checked=v;
 document.body.appendChild(obj_temporal);
 return obj_temporal;
}

function crear_textarea(){
  var obj_temporal=document.createElement('textarea');
  obj_temporal.setAttribute("class","form-control");
  document.body.appendChild(obj_temporal);
  return obj_temporal;
}

function crear_number(tipo,min,max,step){
  var obj_temporal=document.createElement('input');
  obj_temporal.setAttribute("class","form-control");
  document.body.appendChild(obj_temporal);
  obj_temporal.type=tipo;
  obj_temporal.min=min;
  obj_temporal.max=max;
  obj_temporal.step=step;
  return obj_temporal;
}

function crear_checbox(tipo){
  var obj_temporal=document.createElement('input');
  obj_temporal.setAttribute("class","form-control");
  obj_temporal.type=tipo;
  document.body.appendChild(obj_temporal);
  return obj_temporal;  
}

function crear_progress(tipo){
  var obj_temporal=document.createElement('progress');
  obj_temporal.setAttribute("class","form-control");
  obj_temporal.type=tipo;
  document.body.appendChild(obj_temporal);
  return obj_temporal;   
}

function input_color(color){
   var obj_temporal=document.createElement('input');
   obj_temporal.setAttribute("class","form-control");
   obj_temporal.type="color";
   obj_temporal.value=color;
   document.body.appendChild(obj_temporal);
   return obj_temporal;   
}

function crear_lista()
{
    var tipo= document.createElement("select");
    var opg= document.createElement("optgroup");
    opg.label="elementos de la lista";
    var op=document.createElement("option");
    var op2=document.createElement("option")
    var texto1=document.createTextNode("elemento1");
    var texto2=document.createTextNode("elemento1");
    op.appendChild(texto1);
    op2.appendChild(texto2)
    opg.appendChild(op);
    opg.appendChild(op2);
    tipo.appendChild(opg);
    document.body.appendChild(tipo);
}

function crear_fieldset()
{
    var fiel=document.createElement("fieldset");
    var legend=document.createElement("legend");
    var lab= document.createElement("label");
    var input= document.createElement("input");
    var texto_leyenda= document.createTextNode("Legend");
    var texto=document.createTextNode("nombre");
    lab.for="field";
    input.type="text";
    legend.appendChild(texto_leyenda);
    lab.appendChild(texto);
    fiel.appendChild(legend);
    fiel.appendChild(lab)
    fiel.appendChild(input);
    document.body.appendChild(fiel);
    
}

 function crear_tipos(tipo,texto){
   var obj_temporal=document.createElement(tipo);
   var t=document.createTextNode(texto);
   obj_temporal.appendChild(t);
   document.body.appendChild(obj_temporal);
   return obj_temporal;
 }
 
function crear_forma(rejilla){
    obj_form=document.createElement("form");
    obj_form.setAttribute("role","form");
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 1")
    var b=crear_tipos("label","Usuario:");
    var c=crear_input("email");
    var y=crear_tipos("label","Contraseña:");
    var z=crear_input_password("password","password");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_div.appendChild(c);
    obj_div.appendChild(y);
    obj_div.appendChild(z);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_2(rejilla){
    obj_form=document.createElement("form");
    obj_form.setAttribute("role","form");
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 2")
    var b=crear_tipos("label","Nombre:");
    var c=crear_input("email");
    var d=crear_tipos("label","Radio");
    var e=crear_radio("radio");
    var f=crear_sel("radio","true");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_div.appendChild(c);
    obj_div.appendChild(d);
    obj_div.appendChild(e);
    obj_div.appendChild(f);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_3(rejilla){
    obj_form=document.createElement("form");
    obj_form.setAttribute("role","form");
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 3")
    var b=input_color("#e76252");
    var c=crear_input_e("submit");
    var d=crear_input_password("placeholder","placeholder");
    var e=crear_input_e("file");
    var f=input_color("#000000");
    var g=crear_input_e("reset");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_div.appendChild(c);
    obj_div.appendChild(d);
    obj_div.appendChild(e);
    obj_div.appendChild(f);
    obj_div.appendChild(g);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_4(rejilla){
    obj_form=document.createElement("form");
    obj_form.setAttribute("role","form");
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 4")
    var b=crear_input("range");
    var c=crear_input_e("range");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_div.appendChild(c);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_7(rejilla){
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 7")
    var b=crear_textarea();
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    document.body.appendChild(obj_div);
    return obj_form;    
}
    
 function crear_forma_5(rejilla){       
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 5");
    var b=crear_input("number");
    var c=crear_number("number","0","15","3");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_div.appendChild(c);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_6(rejilla){
    obj_form=document.createElement("form");
    obj_form.setAttribute("role","form");
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 6")
    var b=crear_progress("progress");
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    obj_form.appendChild(obj_div);
    document.body.appendChild(obj_form);
    return obj_form;    
}

function crear_forma_7(rejilla){
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var a=crear_tipos("h2","Formulario 7")
    var b=crear_textarea();
    obj_div.appendChild(a);
    obj_div.appendChild(b);
    document.body.appendChild(obj_div);
    return obj_div;    
}

function crear_forma_8(rejilla){
    obj_div=document.createElement("div");
    obj_div.setAttribute("class",rejilla);
    var b=crear_textarea();
    obj_div.appendChild(b);
    document.body.appendChild(obj_div);
    return obj_div;    
}

crear_forma("col-md-4");
crear_forma_2("col-md-4");
crear_forma_3("col-md-4");
crear_forma_4("col-md-8");
crear_forma_5("col-md-4");
crear_forma_6("col-md-6");
crear_forma_7("col-md-6");
crear_forma_8("col-md-1");
